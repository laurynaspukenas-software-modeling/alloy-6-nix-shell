# shell.nix
{ pkgs ? import (builtins.fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/fcc147b1e9358a8386b2c4368bd928e1f63a7df2.tar.gz") # 23.05 channel as of 2023.07.13
  { } }:

pkgs.mkShell { nativeBuildInputs = [ pkgs.alloy6 ]; }
